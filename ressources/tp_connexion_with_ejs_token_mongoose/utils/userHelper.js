import sha512 from "crypto-js/sha512.js";
import fs from "fs";
import {UserModel} from "../models/userModel.js";


export const  userFromLoginAndPassword = async (login, pwd) => {
    //il faut mettre await sans quoi le retour serait une promesse.
    //du coup, il ne faut pas oublier d'ajouter "async" à la fonction qui contient un "await"
    //ne pas oublier que "find" retourne un tableau ou undefined
    return await UserModel.find({
        login: login,
        password: hashPassword(pwd)
    })
        .exec() //exécution de la requête
        .then((users) => {
            //s'il n'y a aucun utilisateur, undefined sera retourné
            //s'il y a un résultat, il ne doit y en avoir qu'un seul
            return users[0]
        })
        .catch(err => {
            console.log(`erreur dans la recherche de l'utilisateur`)
        })
}

export const hashPassword = (pwd) => {
    return sha512(pwd).toString()
}