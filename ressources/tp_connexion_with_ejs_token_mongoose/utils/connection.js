import {dsn} from "../server.js";
import mongoose from "mongoose";

export default async () => {
    mongoose.set('strictQuery', true)
    await mongoose.connect(dsn)
}