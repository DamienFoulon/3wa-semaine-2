//actions en lien avec les utilisateurs
import {dsn} from '../server.js'
import {hashPassword} from "../utils/userHelper.js";
import mongoose from 'mongoose'
import {UserModel} from "../models/userModel.js";


//formulaire d'ajout d'un utilisateur
export const formAddUser = (req, res) => {

    res.render('user/form_user', {
        title: 'ajout utilisateur'
    })
}

export const insertUser = (req, res) => {
    const userPosted = {
        login: req.body.login,
        name: req.body.name,
        password: hashPassword(req.body.password),
        image: req.body.avatar
    }

    async function insert(){
        const newUser = new UserModel(userPosted)

        await newUser.save()

        res.send('utilisateur ajouté')
    }

    insert().catch(err=> {
    //     console.log(err)
        res.send(`Echec de l\'ajout de l'utilisateur : ${err}`)
    //
    })
}

export const listUsers = async (req, res) => {

    const users = await UserModel.find({})
        .exec()
        .then((users)=>{
            return users
        })
        .catch(err => {
            console.log(`erreur dans la recherche de l'utilisateur`)
            res.status(500).send('erreur')
            return
        })
    res.render('user/users_list', {
        title: 'Liste des utilisateurs',
        users: users,
    })

}