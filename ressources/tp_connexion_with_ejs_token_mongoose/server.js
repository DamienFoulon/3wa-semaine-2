import express from 'express'
import dotenv from 'dotenv'
import session from 'express-session'
import router from './routes/userRouter.js'
import ejs from 'ejs'
import connectionBdd from './utils/connection.js'

dotenv.config()

const { APP_LOCALHOST: hostname, APP_PORT: port, APP_SECRET: secret, APP_TOKEN: secretToken, APP_DSN: dsn} = process.env
export {dsn}
export default secretToken

const app = express()


connectionBdd().catch(err => {
    console.log(`pb de connexion à la bdd : ${err}`)
})
//on précise que l'on va utiliser le moteur de template ejs
app.set('view engine', 'ejs')
//on précise où sont situées les "vues"
app.set('views', 'templates')

app.use(express.static("public"))
app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.use(session({
    name: 'authentication',
    secret: secret,
    resave: false,
    saveUninitialized:false,
    cookie: {maxAge: 8640000}
}))
app.listen(port, ()=>{
    console.log(`Application démarrée sur http://${hostname}:${port}`)
})

app.use('/',router)