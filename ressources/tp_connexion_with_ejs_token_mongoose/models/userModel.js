import mongoose from 'mongoose'

const Schema = mongoose.Schema

//il faut décrire la "structure" du document qui doit être manipulé par mongoose
//si mongoDb ne contraint aucune structure, mongoose doit en prévoir une pour chaque objet qu'il gère
const userSchema = new mongoose.Schema({
    login: String,
    name: String,
    password: String,
    image: String
})

//users est le nom de la collection qui va contenir les documents
export const UserModel = mongoose.model('users', userSchema)