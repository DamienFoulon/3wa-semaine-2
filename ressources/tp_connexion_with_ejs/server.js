import express from 'express'
import dotenv from 'dotenv'
import session from 'express-session'
import router from './routes/userRouter.js'
import ejs from 'ejs'

dotenv.config()

const { APP_LOCALHOST: hostname, APP_PORT: port, APP_SECRET: secret} = process.env

const app = express()

//on précise que l'on va utiliser le moteur de template ejs
app.set('view engine', 'ejs')
//on précise où sont situées les "vues"
app.set('views', 'templates')

app.use(express.static("public"))
app.use(express.urlencoded({extended:true}))
app.use(session({
    name: 'authentication',
    secret: secret,
    resave: false,
    saveUninitialized:false,
    cookie: {maxAge: 8640000}
}))
// app.use(express.json)
app.listen(port, ()=>{
    console.log(`Application démarrée sur http://${hostname}:${port}`)
})

app.use('/',router)