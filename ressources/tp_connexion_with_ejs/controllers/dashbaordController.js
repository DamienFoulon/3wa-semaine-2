//dashboard

export default (req, res) => {
    const {image, name} = req.session.user
    res.set('Cache-Control', 'no-store')
    res.render('user/dashboard', {
        'title': 'dashboard',
        'image': image,
        'name': name
    })
}
