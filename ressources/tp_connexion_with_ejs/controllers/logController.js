import fs from 'fs'
import sha512 from 'crypto-js/sha512.js'

//formulaire de connexion

const formLogin = (req, res) => {

    let messageErrorAuthentication = ''

    if (req.session && req.session.user === false) {
        messageErrorAuthentication = '<p class="isError">Votre login et / ou mot de passe sont incorrectes</p>'

        delete req.session.user
    }

    res.render('access/form_login', {
        'title': 'Connexion',
        'messageErrorAuthentication': messageErrorAuthentication
    })
}

const authentication = (req, res) => {
    const {login} = req.body
    let {password} = req.body

    password = sha512(password).toString()

    const users = JSON.parse(fs.readFileSync(`data/users.json`, "utf-8"))

    const user = users.find(user => {
        return user.login === login && user.password === password
    })


    req.session.user = user || false

    if (user) {
        console.log(req.session.user)
        res.redirect('/dashboard')
    } else {
        res.redirect('/login')
    }
}

const logout = (req, res) => {
    req.session.destroy()
    res.redirect('/')
}

export default {formLogin, authentication, logout}