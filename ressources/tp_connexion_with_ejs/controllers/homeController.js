//page d'accueil

export default (req, res) => {
    let content

    if (req.session && req.session.user !== undefined) {
        content = `
        <p><a href="/dashboard">Voir le dashboard</a></p>
        `
    } else {
        content = `<p><a href="/login">Se connecter</a></p>`

    }
    res.render('home', {'title': 'Homepage', 'content': content})
}
