//import du module
// const express = require('express')
import express from "express"

//création d'une application express
const app = express()

//configuration du port utilisé par le serveur
const port = 8080

app.listen(port, () => {
    console.log(`Serveur démarré sur http://localhost:${port}`)
})

app.use(express.static('images'))
app.use(express.static('css'))

//middleware à prévoir pour gérer les données postées
app.use(express.urlencoded({extended: true}))
app.use(express.json())

const m1 = function (req, res, next) {
    console.log(`code de la fonction middleware`)
    next()
}
const m2 = function (req, res, next) {
    console.log(`code de la fonction middleware M2`)
    next()
}

// app.use(m1)
// app.use(m2)


//gestion d'une requête GET pour la route /
app.get('/', (req, res) => {

    res.send(`
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Ajout d'un objet</h1>
<form action="/recup-data" method="post">

    <p>
        <label for="name">Nom de l'objet</label>
        <input type="text" id="name" name="name" value=""/>
    </p>
    <p>
        <label for="color">Couleur de l'objet</label>
        <input type="text" id="color" name="color" value=""/>
    </p>
    <p><input type="submit" value="Envoyer"/></p>
</form>
</body>
</html>
      \`
    `)
})

app.post("/recup-data", (req, res) => {
    const { name, color } = req.body
    console.log(req.body)
    res.send(`L'objet à ajouter a pour nom "${name}" et pour couleur "${color}"  `)
})
