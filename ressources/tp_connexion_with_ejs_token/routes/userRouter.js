import express from 'express'
const router = express.Router()
import HomeController from '../controllers/homeController.js'
import LogController from '../controllers/logController.js'
import DashboardController from '../controllers/dashbaordController.js'
import ReservedController from '../controllers/reservedController.js'
import {checkAuthentication} from "../utils/middlewares.js";


router.get("/", HomeController)
router.get("/login", LogController.formLogin)
router.post("/authentication", LogController.authentication)
router.get("/dashboard",checkAuthentication, DashboardController)
router.get("/logout", LogController.logout)

//pour afficher le formulaire qui permet de s'authentifier pour avoir un token
router.get("/login-token", LogController.formLoginToken)
//route qui retourne un token (après authentitifacation)
router.post("/generateToken", LogController.generateAuthToken)
//page dont l'accès est conditionné à un token
router.get("/reserved", ReservedController)

export default router