import jwt  from "jsonwebtoken"
import secretToken from "../server.js"



export default (req, res) => {


    console.log(req.headers.authorization)

    const token = req.headers.authorization.split(' ')[1]

    try {
        jwt.verify(token, secretToken)
        res.send('Vous avez bien accès a cette page réservée')

    }catch(err) {
        res.status(401).json({message: "Token d'authentification invalide (pas d'accès à la page réservée"})
    }
}