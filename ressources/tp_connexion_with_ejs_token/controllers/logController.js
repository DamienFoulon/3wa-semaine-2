import fs from 'fs'
import sha512 from 'crypto-js/sha512.js'
import jwt  from "jsonwebtoken"
import secretToken from "../server.js"
import {userFromLoginAndPassword} from "../utils/userHelper.js";
//formulaire de connexion

const formLogin = (req, res) => {

    let messageErrorAuthentication = ''

    if (req.session && req.session.user === false) {
        messageErrorAuthentication = '<p class="isError">Votre login et / ou mot de passe sont incorrectes</p>'

        delete req.session.user
    }

    res.render('access/form_login', {
        'title': 'Connexion',
        'titlePage' : 'Se connecter avec une session',
        'isForToken': false,
        'messageErrorAuthentication': messageErrorAuthentication
    })
}

const formLoginToken = (req, res) => {
    console.log(`par ici`)
    let messageErrorAuthentication = ''

    res.render('access/form_login', {
        'title': 'Connexion avec token',
        'titlePage' : `S'authentifier pour récupérer un token`,
        'isForToken': true,
        'messageErrorAuthentication': messageErrorAuthentication
    })
}

//contrôle de l'authentification
const authentication = (req, res) => {
    const {login} = req.body
    let {password} = req.body

    const user = userFromLoginAndPassword(login, password)

    req.session.user = user || false

    if (user) {
        res.redirect('/dashboard')
    } else {
        res.redirect('/login')
    }
}

const generateAuthToken = (req, res) => {

    const {login} = req.body
    let {password} = req.body

    const user = userFromLoginAndPassword(login, password)

    if (user) {
        const token = jwt.sign(
            {
                userId: user.id,
                name: user.name,
                login: user.login
            },
            secretToken,
            {expiresIn: '24h'}
        )

        console.log(`token`, token)

        res.json({token: token})
    } else {
        res.status(401).json({message: 'Erreur d\'authentification'})
    }

}





const logout = (req, res) => {
    req.session.destroy()
    res.redirect('/')
}

export default {formLogin, formLoginToken, authentication, logout, generateAuthToken}