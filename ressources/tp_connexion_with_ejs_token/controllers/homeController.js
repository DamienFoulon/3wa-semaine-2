//page d'accueil

export default (req, res) => {
    let content

    if (req.session && req.session.user !== undefined) {
        content = `
        <p><a href="/dashboard">Voir le dashboard</a></p>
        `
    } else {
        content = `<p><a href="/login">Se connecter avec une session</a></p>
            <p><a href="/login-token">S'authentifier avec un token</a></p>
            `

    }
    res.render('home', {'title': 'Homepage', 'content': content})
}
