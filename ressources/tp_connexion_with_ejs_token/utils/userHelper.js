import sha512 from "crypto-js/sha512.js";
import fs from "fs";

export const userFromLoginAndPassword = (login, pwd) => {

    const password = sha512(pwd).toString()

    const users = JSON.parse(fs.readFileSync(`data/users.json`, "utf-8"))

    return users.find(user => {
        return user.login === login && user.password === password
    })

}