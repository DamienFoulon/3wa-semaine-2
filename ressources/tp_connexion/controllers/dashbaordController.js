//dashboard

export default (req, res) => {
    const {image, name} = req.session.user

    res.set('Cache-Control', 'no-store')

    res.send(
        `
        <html>
           <head>
           <title>Dashboard</title>
           <link rel="stylesheet" href="https://unpkg.com/wingcss"/>
           <link rel="stylesheet" href="css/style.css">
            </head>
            <body>
            <div class="container">
            
            <div class="row">
                <div class="col">
                    <h1>Dashboard</h1>
                        <p><a href="/">Aller sur la page d'accueil</a></p>    
                     <div class="card text-center">
                        <h5 class="card-header">${name}</h5>
                        <p class="card-body">
                        <img src="${image}" alt="avatar">
                        </p>
                        <div class="card-footer">
                            <p><a href="/logout">Se déconnecter</a></p>
                        </div>
                     </div>
                    
                    
                </div>
            </div>
            </div>
            </body>
        </html>
        `
    )
}
