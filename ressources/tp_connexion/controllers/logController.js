import fs from 'fs'
import sha512 from 'crypto-js/sha512.js'


//formulaire de connexion

const formLogin = (req, res) => {

    let messageErrorAuthentication = ''

    if(req.session && req.session.user === false){
        messageErrorAuthentication = '<p class="isError">Votre login et / ou mot de passe sont incorrectes</p>'

        delete req.session.user
    }

    res.send(`
        <html>
           <head>
           <title>Homepage</title>
           <link rel="stylesheet" href="https://unpkg.com/wingcss"/>
           <link rel="stylesheet" href="css/style.css">
            </head>
            <body>
            <div class="container">
            
            <div class="row">
                <div class="col">
                    <h1>Connexion</h1>
                    ${messageErrorAuthentication}
                        <form action="/authentication" method="post">
                            <p>
                                <label for="login">Login: </label>
                                <input type="text" id="login" name="login" value="" />
                            </p>
                            <p>
                                <label for="pass">Mot de passe: </label>
                                <input type="password" id="pass" name="password" value="" />
                            </p>
                            <p><input type="submit" value="demande de connexion" /></p>
                       </form>
                </div>
            </div>
            </div>
            </body>
        </html>
    `)

}

const authentication = (req, res) => {
    const {login} = req.body
    let { password} = req.body

    password = sha512(password).toString()

    const users = JSON.parse(fs.readFileSync(`data/users.json`, "utf-8"))

    const user = users.find(user => {
        return user.login === login && user.password === password
    })



    req.session.user = user || false

    if(user) {
        console.log(req.session.user)
        res.redirect('/dashboard')
    }else{
        res.redirect('/login')
    }
}

const logout = (req, res) => {
    req.session.destroy()
    res.redirect('/')
}


export default {formLogin, authentication, logout}