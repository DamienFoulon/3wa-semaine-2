//page d'accueil

export default (req, res) => {
    let content

    if(req.session && req.session.user !== undefined){
        content = `
        <p><a href="/dashboard">Voir le dashboard</a></p>
        `
    }else{
        content = `<p><a href="/login">Se connecter</a></p>`

    }



    res.send(`
        <html>
           <head>
           <title>Homepage</title>
           <link rel="stylesheet" href="https://unpkg.com/wingcss"/>
           <link rel="stylesheet" href="css/style.css">
            </head>
            <body>
            <div class="container">
            
            <div class="row">
                <div class="col">
                    <h1>Bienvenue !</h1>
                    ${content}
                </div>
            </div>
            </div>
            </body>
        </html>
    `)


}
