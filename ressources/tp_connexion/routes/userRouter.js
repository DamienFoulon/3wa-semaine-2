import express from 'express'
const router = express.Router()
import HomeController from '../controllers/homeController.js'
import LogController from '../controllers/logController.js'
import DashboardController from '../controllers/dashbaordController.js'
import {checkAuthentication} from "../utils/middlewares.js";


router.get("/", HomeController)
router.get("/login", LogController.formLogin)
router.post("/authentication", LogController.authentication)
router.get("/dashboard",checkAuthentication, DashboardController)
router.get("/logout", LogController.logout)

export default router